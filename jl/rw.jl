using Plots
plotlyjs()

n = 400
rw() = cumsum(randn(n))

p = plot3d(rw(), rw(), rw());

PlotlyJS.savehtml(p.o, "rw.html")
