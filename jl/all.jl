using CSV, DataFrames, Measurements, Unitful, Plots, StatsPlots, LaTeXStrings

df = CSV.read("data.csv", types=[Float64, Float64], header=["t", "a"])

df.t = df.t .± 0.3
df.a = df.a .± 0.5

df.t *= u"s"
df.a *= u"m/s^2"

df.s = @. df.a * df.t ^ 2 / 2

show(df)

pgfplots()
PGFPlots.pushPGFPlotsOptions("/pgf/number format/use comma")
@df df plot(
    :t .|> ustrip, :s .|> ustrip, label="Bahnkurve",
    xlab=L"$t$ in $s$", ylab=L"$s$ in $m$",
    seriestype=:scatter, legend=:topleft
)

savefig("plot.svg")
